// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

#include "tbitfield.h"

TBitField::TBitField(int len)
{
	if(len < 0)
		throw exception("length error");
	BitLen = len;
	MemLen = BitLen / (sizeof(TELEM) * 8) + 1;
	pMem = new TELEM[MemLen];
	memset(pMem, 0, sizeof(TELEM) * MemLen);
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.GetLength();
	MemLen = BitLen / (sizeof(TELEM) * 8) + 1;
	pMem = new TELEM[MemLen];
	memcpy(pMem, bf.pMem, sizeof(TELEM) * MemLen);
}

TBitField::~TBitField()
{
	delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	return n / (sizeof(TELEM) * 8);
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	return 1 << (n % (sizeof(TELEM) * 8));
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
	return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	if(n < 0 || n >= BitLen)
		throw exception("wrong index");
	pMem[GetMemIndex(n)] |= GetMemMask(n);
}

void TBitField::ClrBit(const int n) // очистить бит
{
	if(n < 0 || n >= BitLen)
		throw exception("wrong index");
	pMem[GetMemIndex(n)] &= (~GetMemMask(n));
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	if(n < 0 || n >= BitLen)
		throw exception("wrong index");
	return (pMem[GetMemIndex(n)] & GetMemMask(n)) != 0;
}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	delete[] pMem;
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM[MemLen];
	for(int i = 0; i < MemLen; i++)
		pMem[i] = bf.pMem[i];
	return *this;
}

bool TBitField::operator==(const TBitField &bf) const // сравнение
{
	if(GetLength() != bf.GetLength())
		return false;
	for(int i = 0; i < BitLen; i++)
		if(GetBit(i) != bf.GetBit(i))
			return false;
	return true;
}

bool TBitField::operator!=(const TBitField &bf) const // сравнение
{
	return !(*this == bf);
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	TBitField result = TBitField(GetLength() > bf.GetLength() ? GetLength() : bf.GetLength());
	for(int i = 0; i < result.GetLength(); i++) {
		int a = 0;
		int b = 0;
		try {
			a = GetBit(i);
		} catch(exception) {
			a = 0;
		}
		try {
			b = bf.GetBit(i);
		} catch(exception) {
			b = 0;
		}
		if(a || b)
			result.SetBit(i);
		else 
			result.ClrBit(i);
	}
	return result;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	TBitField result = TBitField(GetLength() > bf.GetLength() ? GetLength() : bf.GetLength());
	for(int i = 0; i < result.GetLength(); i++) {
		int a = 0;
		int b = 0;
		try {
			a = GetBit(i);
		} catch(exception) {
			a = 0;
		}
		try {
			b = bf.GetBit(i);
		} catch(exception) {
			b = 0;
		}
		if(a && b)
			result.SetBit(i);
		else 
			result.ClrBit(i);
	}
	return result;
}

TBitField TBitField::operator~(void) // отрицание
{
	TBitField result = TBitField(*this);
	// Лишние биты в выделенной памяти станут единичками
	for(int i = 0; i < MemLen; i++)
		result.pMem[i] = ~result.pMem[i];
	return result;
}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{
	int pos = 0;
	char val = istr.get();
	while(val != '\n') {
		switch(val) {
		case '0':
			bf.ClrBit(pos);
			break;
		case ' ':
			pos--;
			break;
		case '1':
			bf.SetBit(pos);
			break;
		default:
			return istr;
		}
		pos++;
		val = istr.get();
	}
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
	for(int i = 0; i < bf.GetLength(); i++)
		ostr << bf.GetBit(i);
	return ostr;
}
